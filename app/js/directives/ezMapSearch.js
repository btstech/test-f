angular.module('ezerowaste')

/**
* Renders a map search box.
*/
.directive('ezMapSearch', function(dialogService, uiGmapGoogleMapApi) {
  'use strict';

  return {
    restrict: 'E',
    scope: {
      place: '=',
      map: '&'
    },
    templateUrl: 'templates/map-search-input.html',
    link: function(scope, iElement, iAttrs) {
      scope.searchText = '';

      scope.openSearchModal = function() {
        uiGmapGoogleMapApi.then(function(maps) {
          var map = scope.map();

          if (map == null) {
            return;
          }

          var dialogScope = scope.$new();

          dialogScope.data = {query: scope.searchText};
          dialogScope.results = [];

          var autocompleteService = new maps.places.AutocompleteService();
          var placesService = new maps.places.PlacesService(map);

          dialogScope.updateResults = function() {
            var query = dialogScope.data.query;

            if (!query) {
              return;
            }

            autocompleteService.getPlacePredictions({
              input: query,
              bounds: map.getBounds()
            }, function(predictions, status) {
              if (status !== maps.places.PlacesServiceStatus.OK) {
                return;
              }

              dialogScope.results = predictions;

              dialogScope.$apply();
            });
          };

          dialogScope.selectResult = function(result) {
            if (result == null) {
              return;
            }

            placesService.getDetails({
              placeId: result['place_id']
            }, function(place, status) {
              if (status !== maps.places.PlacesServiceStatus.OK) {
                return;
              }

              dialogScope.close(place);
              dialogScope.$apply();
            });
          };

          dialogScope.updateResults();

          dialogService.fromTemplateUrl('templates/map-search-dialog.html', {
            scope: dialogScope,
            focusFirstInput: true
          }).then(function(place) {
            scope.place = place;
            scope.searchText = place.name;
          });
        });
      };
    }
  };
});
