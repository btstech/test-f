angular.module('ezerowaste')

/**
* Renders an action type.
*/
.directive('ezActionText', function() {
  'use strict';

  return {
    restrict: 'E',
    scope: {
      action: '&'
    },
    templateUrl: 'templates/action-text-template.html'
  };
});
