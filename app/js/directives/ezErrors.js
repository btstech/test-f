angular.module('ezerowaste')

/**
* Renders a list of errors (strings).
*/
.directive('ezErrors', function() {
  'use strict';

  return {
    restrict: 'E',
    scope: {
      source: '&'
    },
    template: '<div ng-repeat="error in source()">{{error}}</div>'
  };
});
