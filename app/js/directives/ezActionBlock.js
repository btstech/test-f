angular.module('ezerowaste')

/**
* Renders an action in a block with an icon and text.
*/
.directive('ezActionBlock', function() {
  'use strict';

  return {
    restrict: 'E',
    scope: {
      action: '&'
    },
    templateUrl: 'templates/action-block-template.html',
    link: function(scope, iElement, attrs, form) {
      scope.mappedAction = function() {
        var actionType = scope.action();
        switch (actionType) {
          case 'Recycle':
            return 'recycle';
          case 'Repair':
          case 'Swap':
            return 'reuse';
          case 'Donate':
            return 'reduce';
          default:
            throw new Error('Unhandled action value: ' + actionType);
        }
      };
    }
  };
});
