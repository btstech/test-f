angular.module('ezerowaste')

/**
* Renders an electronic device.
*/
.directive('ezDevice', function() {
  'use strict';

  return {
    restrict: 'E',
    scope: {
      device: '&',
      size: '@?',
      hideText: '&?'
    },
    templateUrl: 'templates/device-template.html',
    link: function(scope, iElement, iAttrs) {
      if (scope.size) {
        iElement.children().addClass('ez-device-block-' + scope.size);
      }
    }
  };
});
