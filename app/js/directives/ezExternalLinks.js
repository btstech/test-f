angular.module('ezerowaste')

/**
* Opens external links in the native browser.
*/
.directive('ezExternalLinks', function($window, $ionicPlatform) {
  'use strict';

  return {
    restrict: 'A',
    link: function(scope, iElement, iAttrs) {
      iElement.on('click', function(event) {
        var target = event.target;
        var externalProtocolsRegex = /^\s*(https?):/i;

        if (target.tagName.toUpperCase() === 'A' && externalProtocolsRegex.test(target.href)) {
          $ionicPlatform.ready(function() {
            $window.cordova.InAppBrowser.open(target.href, '_system');
          });
          event.preventDefault();
        }
      });
    }
  };
});
