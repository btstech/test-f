angular.module('ezerowaste')

/**
* Adds a validation error container as a sibling of the target element.
*/
.directive('ezValidate', function($compile, $templateRequest) {
  'use strict';

  return {
    restrict: 'A',
    require: '^form',
    scope: {
      serverErrors: '&',
      ezValidate: '&',
      messagesSource: '@'
    },
    link: function(scope, iElement, attrs, form) {
      scope.hasErrors = function() {
        var model = scope.ezValidate();
        return scope.hasServerSideErrors() ||
              (model && model.$invalid && (model.$dirty || form.$submitted));
      };

      scope.hasServerSideErrors = function() {
        var serverErrors = scope.serverErrors();
        return serverErrors && serverErrors.length > 0;
      };

      var baseMessagesSource = 'templates/error-messages.html';

      var errorContainer = angular.element('<div></div>');
      errorContainer.attr({
        'class': 'error-list padding',
        'ng-if': 'hasErrors()'
      });

      var clientErrorsContainer = angular.element('<div></div>');
      clientErrorsContainer.attr({
        'ng-if': '!hasServerSideErrors()',
        'ng-messages': 'ezValidate().$error'
      });

      var baseMessagesContainer = angular.element('<div></div>');
      baseMessagesContainer.attr({
        'ng-messages-include': baseMessagesSource
      });

      var overridenMessagesContainer = angular.element('<div></div>');
      overridenMessagesContainer.attr({
        'ng-messages-include': scope.messagesSource
      });

      clientErrorsContainer.append(overridenMessagesContainer);
      clientErrorsContainer.append(baseMessagesContainer);
      errorContainer.append(clientErrorsContainer);

      var serverErrorsContainer = angular.element(
        '<ez-errors source="serverErrors()"></ez-errors>');
      errorContainer.append(serverErrorsContainer);

      iElement.after(errorContainer);
      $compile(errorContainer)(scope);
    }
  };
});
