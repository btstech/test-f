angular.module('ezerowaste')

/**
* Renders an action status.
*/
.directive('ezActionStatus', function() {
  'use strict';

  return {
    restrict: 'E',
    scope: {
      status: '&'
    },
    templateUrl: 'templates/action-status-template.html'
  };
});
