angular.module('ezerowaste')

/**
* Represents a radio group (a group of buttons with one choice).
*/
.directive('ezRadioGroup', function() {
  'use strict';

  return {
    restrict: 'A',
    scope: {
      ezModel: '=',
      ezRadioGroup: '&'
    },
    link: function(scope, iElement, iAttrs) {
      iElement.bind('click', function() {
        scope.ezModel = scope.ezRadioGroup();
        scope.$apply();
      });

      scope.$watch('ezModel', function() {
        if (scope.ezModel === scope.ezRadioGroup()) {
          iElement.addClass('selected');
        } else {
          iElement.removeClass('selected');
        }
      });
    }
  };
});
