angular.module('ezerowaste.controllers')

.controller('StatisticsController', function($scope, apiService, screenshotService,
  $cordovaSocialSharing, chainedLoading, $ionicPopup, gettextCatalog) {
  'use strict';

  function reset() {
    $scope.statistics = null;
  }

  $scope.requestError = false;
  reset();

  $scope.reloadStatistics = function() {
    reset();

    apiService.get('Statistics').then(function(response) {
      $scope.requestError = false;
      $scope.statistics = response.data;
    }, function() {
      $scope.requestError = true;
      $scope.statistics = null;
    });
  };

  $scope.$on('$ionicView.beforeEnter', function() {
    $scope.reloadStatistics();
  });

  $scope.share = function() {
    chainedLoading.show();
    screenshotService.takeScreenshot().then(function(uri) {
      return $cordovaSocialSharing.share(null, 'E-Zerowaste', uri);
    }, function(error) {
      $ionicPopup.alert({
        title: gettextCatalog.getString('Error'),
        template: gettextCatalog.getString(
          'We were unable to share your statistics, try again later')
      });
    }).finally(function() {
      chainedLoading.hide();
    });
  };
});
