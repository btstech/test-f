angular.module('ezerowaste.controllers')

.controller('HistoryController', function($scope, apiService, $ionicPopup, gettextCatalog, $q) {
  'use strict';

  function reset() {
    $scope.actions = [];
    $scope.isFirstRequest = true;
    $scope.requestError = false;
    $scope.pageMarker = null;
    $scope.listEndReached = false;

    $scope.deviceMap = null;
  }

  reset();

  $scope.loadMoreActions = function() {
    return apiService.get('ActionEntries', {
      params: {
        pageMarker: $scope.pageMarker
      },
      // The infinite-scroll has a loading icon, so we disable the global one except for the first
      // request
      skipLoadingInterceptor: !$scope.isFirstRequest
    }).then(function(response) {
      $scope.isFirstRequest = false;
      $scope.requestError = false;

      var newItems = response.data.items;
      $scope.listEndReached = !newItems.length;
      $scope.actions = $scope.actions.concat(newItems);
      $scope.pageMarker = response.data.pageMarker;
    }, function(reason) {
      $scope.requestError = true;
      if (!$scope.isFirstRequest) {
        return $ionicPopup.alert({
          title: gettextCatalog.getString('Error'),
          template: gettextCatalog.getString('We were unable to fetch more entries at this time')
        });
      }
      return $q.reject(reason);
    }).finally(function() {
      $scope.$broadcast('scroll.infiniteScrollComplete');
    });
  };

  $scope.reloadActions = function() {
    reset();

    apiService.get('ElectronicDevices').then(function(response) {
      $scope.deviceMap = _.keyBy(response.data, 'id');

      return $scope.loadMoreActions();
    }, function() {
      $scope.requestError = true;
    });
  };

  $scope.$on('$ionicView.beforeEnter', function() {
    $scope.reloadActions();
  });

  $scope.showDetails = function(action) {
    var popupScope = $scope.$new();
    popupScope.action = action;

    $ionicPopup.alert({
      title: gettextCatalog.getString('Details'),
      templateUrl: 'action-info-template.html',
      scope: popupScope
    });
  };

  $scope.cancelAction = function(index) {
    var action = $scope.actions[index];
    if (!action) {
      return;
    }

    apiService.delete('ActionEntries/' + action.id).then(function(response) {
      $scope.actions.splice(index, 1);
    }, function() {
      $ionicPopup.alert({
        title: gettextCatalog.getString('Error'),
        template:
          gettextCatalog.getString('We were unable to delete the requested item at this time')
      });
    });
  };
});
