angular.module('ezerowaste.controllers')

.controller('AboutController', function($scope, appInfo) {
  'use strict';

  $scope.appInfo = appInfo;
});
