angular.module('ezerowaste.controllers')

.controller('SettingsController', function($scope, appSettings, appInfo, i18nService) {
  'use strict';

  $scope.settings = appSettings;
  $scope.languages = i18nService.getAvailableLanguages();
  $scope.appInfo = appInfo;
});
