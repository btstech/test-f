angular.module('ezerowaste.controllers')

.controller('MainController', function($scope, $location, authService, loginHandler, $q,
  $ionicHistory) {
  'use strict';

  $scope.requestError = false;

  $scope.automaticLogin = function() {
    $scope.requestError = false;
    var loggedInPromise = authService.isLoggedIn() ? $q.resolve() :
      loginHandler.tryAutomaticLogin();

    loggedInPromise.then(function() {
      $ionicHistory.nextViewOptions({
        disableBack: true,
        disableAnimate: true
      });
      $location.path('app/processDevice');
    }, function() {
      $scope.requestError = true;
    });
  };

  $scope.$on('$ionicView.enter', function() {
    $scope.automaticLogin();
  });
});
