angular.module('ezerowaste.controllers')

.controller('NewsController', function($scope, apiService) {
  'use strict';

  $scope.news = null;
  $scope.newsRequestError = false;

  $scope.reloadNews = function() {
    apiService.get('Announcements').then(function(response) {
      $scope.newsRequestError = false;
      $scope.news = response.data;

      _.each($scope.news, function(item) {
        item.publishedDate = Date.parse(item.publishedDate);
      });
    }, function() {
      $scope.newsRequestError = true;
      $scope.news = null;
    });
  };

  $scope.$on('$ionicView.beforeEnter', function() {
    $scope.reloadNews();
  });
});
