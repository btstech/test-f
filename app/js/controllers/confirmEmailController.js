angular.module('ezerowaste.controllers')

.controller('ConfirmEmailController', function($scope, $http, $ionicPopup, apiService,
  gettextCatalog, dialogService) {
  'use strict';

  function reset() {
    $scope.data = {
      token: ''
    };
    $scope.serverErrors = {};
  }

  reset();

  $scope.sendConfirmationEmail = function() {
    apiService.post('Account/ConfirmationEmailRequest').then(function() {
      $ionicPopup.alert({
        title: gettextCatalog.getString('Activation code sent'),
        template: gettextCatalog.getString(
          'We sent an activation code to your mail. Please check your inbox.')
      });
    }, function() {
      $ionicPopup.alert({
        title: gettextCatalog.getString('Error'),
        template: gettextCatalog.getString(
          'We had an error while sending your activation code. Please try again later.')
      });
    });
  };

  $scope.confirmEmail = function() {
    apiService.post('Account/ConfirmEmail', $scope.data).then(function() {
      $scope.close();
      reset();
    }, function(response) {
      if (response.status === 400) {
        $scope.serverErrors = response.data.errors;
      }

      $ionicPopup.alert({
        title: gettextCatalog.getString('Error'),
        template: gettextCatalog.getString(
          'We were unable to activate your account with the provided code. Please ensure it is ' +
          'correct and try again.')
      });
    });
  };
});
