angular.module('ezerowaste.controllers')

.controller('SignUpController', function($scope, $http, $ionicPopup, apiService, gettextCatalog) {
  'use strict';

  function reset() {
    $scope.userData = {
      firstName: '',
      lastName: '',
      email: '',
      birthday: null,
      gender: '',
      password: ''
    };
    $scope.serverErrors = {};
  }

  reset();

  $scope.save = function() {
    apiService.post('Account/Register', $scope.userData)
    .success(function() {
      $ionicPopup.alert({
        title: gettextCatalog.getString('Success'),
        template: gettextCatalog.getString('Successfully registered')
      }).then(function() {
        // Resolve the promise with credentials so the caller can immediately login.
        // Ideally, we would get an access token directly, but this is not implemented yet.
        $scope.close({
          userName: $scope.userData.email,
          password: $scope.userData.password
        });
        reset();
      });
    }).error(function(data, status, headers, config) {
      var text;
      if (status === 400) {
        $scope.serverErrors = data.errors;
        text = gettextCatalog.getString(
          'There were some problems while saving your profile, please check them and try again');
      } else {
        text = gettextCatalog.getString(
          'We had a problem while processing your request. Check your connection and try again');
      }
      $ionicPopup.alert({
        title: gettextCatalog.getString('Error'),
        template: text
      });
    });
  };
});
