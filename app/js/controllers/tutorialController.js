angular.module('ezerowaste.controllers')

.controller('TutorialController', function($scope, $state, appSettings, dialogService,
  authService, loginHandler, $ionicSlideBoxDelegate) {
  'use strict';

  // Mark tutorial as shown
  appSettings.tutorialShown = true;

  $scope.currentSlide = 0;
  $scope.user = null;

  $scope.goToMain = function() {
    $state.go('app.main');
  };

  function afterLogin() {
    $ionicSlideBoxDelegate.slide(1);
  }

  $scope.signUp = function() {
    dialogService.fromTemplateUrl('templates/signup.html', {}).then(function(credentials) {
      return authService.loginWithPassword(credentials.userName, credentials.password);
    }).then(afterLogin);
  };

  $scope.login = function() {
    loginHandler.requestLogin().then(afterLogin);
  };

  $scope.isLoggedIn = function() {
    return authService.isLoggedIn();
  };

  $scope.logout = function() {
    return authService.logout();
  };

  $scope.$watch(function() {
    return authService.currentUser;
  }, function() {
    $scope.user = authService.currentUser;
  });
});
