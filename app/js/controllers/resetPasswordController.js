angular.module('ezerowaste.controllers')

.controller('ResetPasswordController', function($scope, $http, $ionicPopup, apiService,
  gettextCatalog, dialogService) {
  'use strict';

  function reset() {
    $scope.data = {
      email: '',
      password: '',
      token: ''
    };
    $scope.serverErrors = {};
  }

  reset();

  $scope.requestResetCode = function() {
    apiService.post('Account/PasswordResetRequest', {
      email: $scope.data.email
    }).then(function() {
      $ionicPopup.alert({
        title: gettextCatalog.getString('Activation code sent'),
        template: gettextCatalog.getString(
          'If your mail address is correct and you are an existing user, we will send a reset ' +
          'code in the following minutes. Please check your inbox.')
      });
    }, function() {
      $ionicPopup.alert({
        title: gettextCatalog.getString('Error'),
        template: gettextCatalog.getString(
          'We had an error while sending your reset code. Please try again later.')
      });
    });
  };

  $scope.resetPassword = function() {
    apiService.post('Account/ResetPassword', $scope.data).then(function() {
      $scope.close({
        userName: $scope.data.email,
        password: $scope.data.password
      });
      reset();
    }, function(response) {
      if (response.status === 400) {
        $scope.serverErrors = response.data.errors;
      }

      $ionicPopup.alert({
        title: gettextCatalog.getString('Error'),
        template: gettextCatalog.getString(
          'We were unable to reset your password with the provided code. Please ensure it is ' +
          'correct and try again.')
      });
    });
  };
});
