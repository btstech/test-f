angular.module('ezerowaste.controllers')

.controller('LoginController', function($scope, $http, $ionicPopup, authService, gettextCatalog,
  dialogService, chainedLoading) {
  'use strict';

  function reset() {
    $scope.credentials = {
      userName: '',
      password: ''
    };
    $scope.serverErrors = {};
  }

  reset();

  function loginSuccessful() {
    $scope.close();
    reset();
  }

  function login(credentials) {
    authService.loginWithPassword(credentials.userName, credentials.password)
    .then(loginSuccessful, function(response) {
      var text;
      if (response.status === 400) {
        $scope.serverErrors = response.data.errors;
      }
      $ionicPopup.alert({
        title: gettextCatalog.getString('Error'),
        template: gettextCatalog.getString(
          'Unable to sign in. Please check your credentials and try again')
      });
    });
  }

  $scope.signUp = function() {
    if (chainedLoading.isShown() || $scope.isClosing()) {
      return;
    }
    dialogService.fromTemplateUrl('templates/signup.html', {}).then(function(credentials) {
      login(credentials);
    });
  };

  $scope.resetPassword = function() {
    if (chainedLoading.isShown() || $scope.isClosing()) {
      return;
    }
    dialogService.fromTemplateUrl('templates/reset-password.html', {}).then(function(credentials) {
      login(credentials);
    });
  };

  $scope.login = function() {
    if (chainedLoading.isShown() || $scope.isClosing()) {
      return;
    }
    login($scope.credentials);
  };

  $scope.loginWithFacebook = function() {
    if (chainedLoading.isShown() || $scope.isClosing()) {
      return;
    }
    chainedLoading.show();
    authService.loginWithFacebook().then(loginSuccessful, function() {
      $ionicPopup.alert({
        title: gettextCatalog.getString('Error'),
        template: gettextCatalog.getString(
          'We were not able to log in with your Facebook account, please try again later')
      });
    }).finally(function() {
      chainedLoading.hide();
    });
  };
});
