// TODO: Consider dividing this controller

angular.module('ezerowaste.controllers')

.controller('ProcessDeviceController', function($scope, $state, $ionicHistory,
  apiService, ecopointsService, $window, chainedLoading, $ionicPopup, uiGmapGoogleMapApi,
  gettextCatalog, $cordovaSocialSharing, $cordovaCamera, $templateRequest, $compile,
  uiGmapIsReady, $cordovaGeolocation, $ionicPlatform, mapsReloader, screenshotService) {
  'use strict';

  $scope.ecopoints = [];
  $scope.devices = null;
  $scope.deviceRequestError = false;
  $scope.brands_filter_opts = {};

  $scope.reloadDevices = function() {
    apiService.get('ElectronicDevices').then(function(response) {
      $scope.deviceRequestError = false;
      $scope.devices = response.data;
    }, function() {
      $scope.deviceRequestError = true;
      $scope.devices = null;
    });
  };

  $scope.reloadDevices();

  $scope.$on('gettextLanguageChanged', function() {
    if($scope.brands_filter_opts.type_options
      && $scope.brands_filter_opts.type_options[0]
      && $scope.brands_filter_opts.type_options[0].id == -1){
      $scope.brands_filter_opts.type_options[0].name = gettextCatalog.getString('Select a brand');
    }
    $scope.reloadDevices();
  });

  $scope.deviceData = {
    device: null,
    isWorking: false,
    action: 'Recycle',
    ecopoint: null
  };

  $scope.resultCode = null;

  $scope.data = {
    myLocation: null,
    place: null,
    swapPictureUrl: null,
    isMarkerWindowShown: false,
    markerWindowOptions: null,
    locationMarkerOptions: null,
    filteredBrand: null
  };

  $scope.map = {
    location: {latitude: 4.684672615315212, longitude: -74.17746752513914},
    zoom: 3,
    pan: null,
    control: {},
    markersControl: {}
  };

  function initializeMap() {
    // Save old location first to set it again after resize

    var oldLocation = angular.copy($scope.map.location);

    uiGmapIsReady.promise().then(function() {
      // Add control manually to associate it with this scope.
      var gmap = $scope.map.control.getGMap();

      $templateRequest('my-location-button.html').then(function(response) {
        var compiledContent = $compile(response)($scope.$new());
        var container = angular.element('<div/>');
        container.append(compiledContent);
        container[0].index = 1;

        gmap.controls[google.maps.ControlPosition.LEFT_BOTTOM].clear();
        gmap.controls[google.maps.ControlPosition.LEFT_BOTTOM].push(container[0]);
      });

      uiGmapGoogleMapApi.then(function(maps) {
        maps.event.trigger(gmap, 'resize');
        $scope.map.control.refresh(oldLocation);
      });
    });
  }

  $scope.$watch('data.place', function(place) {
    if (place && place.geometry) {
      var location = place.geometry.location;
      $scope.map.location = {
        latitude: location.lat(),
        longitude: location.lng()
      };
      $scope.map.zoom = 15;
    }
  });

  $scope.$on('$ionicView.enter', function() {
    $scope.mapsLoaded = false;
    if ($ionicHistory.currentStateName() !== 'app.processDevice.3-SelectAction') {
      return;
    }

    uiGmapGoogleMapApi.then(function() {
      $scope.mapsLoaded = true;
      $scope.data.markerWindowOptions = {
        pixelOffset: new google.maps.Size(0, -40)
      };

      $scope.data.locationMarkerOptions = {
        icon: {
          url: 'img/location-me.png',
          scaledSize: new google.maps.Size(40, 40)
        }
      };

      initializeMap();
    });
  });

  $scope.onMarkerClicked = function(gMarker, eventName, model) {
    if ($scope.deviceData.ecopoint && model.id === $scope.deviceData.ecopoint.id) {
      $scope.data.isMarkerWindowShown = !$scope.data.isMarkerWindowShown;
      return;
    }

    $scope.deviceData.ecopoint = model;
    $scope.data.isMarkerWindowShown = true;
  };

  $scope.closeMarkerWindow = function() {
    $scope.data.isMarkerWindowShown = false;
  };

  $scope.$watchGroup(['map.location.latitude', 'map.location.longitude', 'deviceData.action'],
  function() {
    updateMarkers();
  });

  $scope.$watch('deviceData.action',
  function() {
    $scope.deviceData.ecopoint = null;
    $scope.deviceData.isMarkerWindowShown = false;

    // Trigger map initialization as it's removed when the action changes.
    initializeMap();
  });

  $scope.reloadMaps = function() {
    mapsReloader.reload();
  };

  $scope.selectDevice = function(value) {
    $scope.deviceData.device = value;
    $state.go('app.processDevice.2-SelectStatus');
  };

  $scope.selectWorking = function(value) {
    $scope.deviceData.isWorking = value;
    $state.go('app.processDevice.3-SelectAction');
  };

  $scope.cleanBrandFilter = function() {
    $scope.data.filteredBrand = null;
  };

  $scope.selectEcopoint = function(ecopoint) {
    $scope.deviceData.ecopoint = ecopoint;

    var deviceStatus = $scope.deviceData.isWorking ? 'Working' : 'NotWorking';

    apiService.post('ActionEntries', {
      actionType: $scope.deviceData.action,
      deviceId: $scope.deviceData.device.id,
      ecopointId: $scope.deviceData.ecopoint.id,
      deviceStatus: deviceStatus
    }).then(function(response) {
      $scope.resultCode = response.data.id;
      $state.go('app.processDevice.4-Result');
    }, function(error) {
      $ionicPopup.alert({
        title: gettextCatalog.getString('Error'),
        template: gettextCatalog.getString(
          'We were unable to process your request. Please try again later')
      });
    });
  };

  $scope.showStatistics = function() {
    $state.go('app.processDevice.5-Statistics');
  };

  $scope.shareStatistics = function() {
    chainedLoading.show();
    screenshotService.takeScreenshot().then(function(uri) {
      return $cordovaSocialSharing.share(null, 'E-Zerowaste', uri);
    }, function(error) {
      $ionicPopup.alert({
        title: gettextCatalog.getString('Error'),
        template: gettextCatalog.getString(
          'We were unable to share your statistics, try again later')
      });
    }).finally(function() {
      chainedLoading.hide();
    });
  };

  $scope.goHome = function() {
    $ionicHistory.nextViewOptions({
      historyRoot: true
    });
    $state.go('app.processDevice.1-SelectDevice');
  };

  function updateMarkers() {
    var location = $scope.map.location;

    if (!location || $scope.deviceData.action === 'Swap') {
      return;
    }
    uiGmapGoogleMapApi.then(function() {
      return ecopointsService.getNearbyEcopoints($scope.deviceData.action, location, $scope.data.filteredBrand);
    })
    .then(function(result) {
      var oldEcopoints = $scope.ecopoints;

      $scope.ecopoints = result.ecopoints;

      _.each($scope.ecopoints, function(ecopoint) {
        // Set up icon
        ecopoint._icon = {
          url: 'img/marker-' + $scope.deviceData.action + '.png',
          scaledSize: new google.maps.Size(40, 40)
        };
      });

      var forceRedraw = !angular.equals($scope.ecopoints, oldEcopoints);
      // The map has problems updating the icon. We force a full rebuild.
      if (forceRedraw && $scope.map.markersControl.newModels) {
        $scope.map.markersControl.newModels($scope.ecopoints);
      }
    });
  }

  updateMarkers();

  $scope.getMyLocation = function(options) {
    options = angular.extend({
      showErrorMessage: true,
      showLoading: true
    }, options);

    if (options.showLoading) {
      chainedLoading.show();
    }

    $ionicPlatform.ready().then(function() {
      return $cordovaGeolocation.getCurrentPosition({
        timeout: 5000
      });
    }).then(function(position) {
        $scope.data.myLocation = {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude
        };
        $scope.map.zoom = 17;

        $scope.map.location = angular.copy($scope.data.myLocation);
      })
      .catch(function(error) {
        if (options.showErrorMessage) {
          $ionicPopup.alert({
            title: gettextCatalog.getString('Error'),
            template: gettextCatalog.getString('Unable to get your position at this time. Make ' +
              'sure you have enabled location services and try again.')
          });
        }
      })
      .finally(function() {
        if (options.showLoading) {
          chainedLoading.hide();
        }
      });
  };

  $scope.getBrands = function(){
    ecopointsService.getBrands().then(function(resp){
      //console.log("BRANDS", resp.data);
      $scope.brands_filter_opts.type_options = resp.data;
      $scope.brands_filter_opts.type_options.splice(0, 0, {name: gettextCatalog.getString('Select a brand'), id: -1});
      $scope.data.filteredBrand = $scope.brands_filter_opts.type_options[0];
    });
  };

  $scope.onBrandSelected = function() {
    updateMarkers();
  };

  $ionicPlatform.ready(function() {
    function getPictureOptions() {
      return {
        quality: 100,
        destinationType: $window.Camera.DestinationType.FILE_URL,
        allowEdit: true,
        encodingType: $window.Camera.EncodingType.JPEG,
        saveToPhotoAlbum: false
      };
    }

    function setPicture(imageData) {
      $scope.data.swapPictureUrl = imageData;
    }

    function retrievePicture(options) {
      return $cordovaCamera.getPicture(options).then(function(imageData) {
        setPicture(imageData);
      }, function(err) {
        if (err && err.indexOf('cancelled') === -1) {
          $ionicPopup.alert({
            title: gettextCatalog.getString('Error'),
            template: gettextCatalog.getString('Unable to get picture')
          });
        }
      });
    }

    $scope.takePicture = function() {
      retrievePicture(angular.extend({}, getPictureOptions(), {
        sourceType: $window.Camera.PictureSourceType.CAMERA
      }));
    };

    $scope.selectPicture = function() {
      retrievePicture(angular.extend({}, getPictureOptions(), {
        sourceType: $window.Camera.PictureSourceType.SAVEDPHOTOALBUM
      }));
    };

    $scope.sharePicture = function() {
      if ($scope.data.swapPictureUrl == null) {
        return;
      }

      $cordovaSocialSharing
        .share(
          gettextCatalog.getString('I am a Z-waster! I help the planet by exchanging devices I ' +
            'don\'t use anymore. If you\'re interested, contact me.'),
          gettextCatalog.getString('Device exchange'),
          $scope.data.swapPictureUrl)
        .then(undefined, function(err) {
          $ionicPopup.alert({
            title: gettextCatalog.getString('Error'),
            template: gettextCatalog.getString(
              'We were unable to share this device. Please try again')
          });
        });
    };
  });

  $scope.getMyLocation({
    showLoading: false,
    showErrorMessage: false
  });
});
