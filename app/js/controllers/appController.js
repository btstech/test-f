angular.module('ezerowaste.controllers')

.controller('AppController', function($scope, $state, $timeout, appSettings, authService,
    $ionicSideMenuDelegate, loginHandler) {
  'use strict';

  $scope.fullName = '';
  $scope.user = null;

  $scope.isLoggedIn = function() {
    return authService.isLoggedIn();
  };

  $scope.logout = function() {
    authService.logout().then(function() {
      $state.go('app.main');
    });
  };

  $scope.login = function() {
    loginHandler.requestLogin();
  };

  $scope.$watch(function() {
    return authService.currentUser;
  }, function() {
    $scope.user = authService.currentUser;
    if ($scope.user) {
      $scope.fullName = $scope.user.firstName + ' ' + $scope.user.lastName;
    } else {
      $scope.fullName = '';
    }
  });

  if (!appSettings.tutorialShown && !$state.is('tutorial')) {
    // Show the tutorial on the first run
    $state.go('tutorial');
  }
});
