angular.module('ezerowaste.services')

/**
* Encapsulates API interaction logic
*/
.factory('apiService', function($http, apiUrl) {
  'use strict';

  return {
    get: function(endpoint, config) {
      return $http.get(apiUrl + endpoint, config);
    },
    post: function(endpoint, data, config) {
      return $http.post(apiUrl + endpoint, data, config);
    },
    put: function(endpoint, data, config) {
      return $http.put(apiUrl + endpoint, data, config);
    },
    delete: function(endpoint, config) {
      return $http.delete(apiUrl + endpoint, config);
    },
    isApiRequest: function(url) {
      return url.lastIndexOf(apiUrl, 0) !== -1;
    }
  };
});
