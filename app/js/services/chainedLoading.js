angular.module('ezerowaste.services')

/**
* Wraps $ionicLoading to allow multiple (chained) calls to show() and hide()
*/
.factory('chainedLoading', function($ionicLoading) {
  'use strict';

  var remainingShown = 0;

  return {
    show: function(options) {
      ++remainingShown;
      $ionicLoading.show(options);
    },
    hide: function() {
      if (remainingShown === 0) {
        return;
      }

      --remainingShown;
      if (remainingShown === 0) {
        $ionicLoading.hide();
      }
    },
    isShown: function() {
      return remainingShown > 0;
    }
  };
});
