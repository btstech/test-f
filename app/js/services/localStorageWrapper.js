angular.module('ezerowaste.services')

/**
* Allows to read and write objects to localStorage
*/
.factory('localStorageWrapper', function($window) {
  'use strict';

  return {
    getKey: function(key) {
      var jsonString = $window.localStorage.getItem(key);
      return jsonString ? $window.angular.fromJson(jsonString) : undefined;
    },
    setKey: function(key, value) {
      $window.localStorage.setItem(key, $window.angular.toJson(value));
    }
  };
});
