angular.module('ezerowaste.services')

/**
* Injects the current token in the Authorization header.
*/
.factory('authorizationHeaderInterceptor', function($injector, $q) {
  'use strict';

  return {
    'request': function(config) {
      if (config.skipAuthorizationHeaderInterceptor) {
        return config;
      }

      var authService = $injector.get('authService');
      var apiService = $injector.get('apiService');

      if (apiService.isApiRequest(config.url)) {
        config.headers.Authorization = 'Bearer ' + authService.token;
      }
      return config;
    }
  };
})

/**
* Logins and retries the request when the server answers with 401 Unauthorized.
*/
.factory('unauthorizedInterceptor', function($injector, $q) {
  'use strict';

  return {
    'responseError': function(rejection) {
      var apiService = $injector.get('apiService');

      if (apiService.isApiRequest(rejection.config.url) && rejection.status === 401) {
        var loginHandler = $injector.get('loginHandler');
        var ezHelpers = $injector.get('ezHelpers');

        // Attempt to login and, if successful, try again. Otherwise, reject
        // the promise directly
        return ezHelpers.retryRequestWhenPromiseIsResolved(loginHandler.requestLogin(),
          rejection);
      }

      return $q.reject(rejection);
    }
  };
})

/**
* Intercepts unreachable HTTP requests.
*/
.factory('unreachableInterceptor', function($injector, $q) {
  'use strict';

  return {
    'responseError': function(rejection) {
      if (rejection.status <= 0) {
        var $ionicPopup = $injector.get('$ionicPopup');
        var gettextCatalog = $injector.get('gettextCatalog');
        var $http = $injector.get('$http');
        var ezHelpers = $injector.get('ezHelpers');

        var popUpPromise = ezHelpers.allowOneActivePromise('interceptors.unreachable', function() {
          return $ionicPopup.confirm({
            title: gettextCatalog.getString('Connection error'),
            template: gettextCatalog.getString('We were unable to reach ' +
              'E-Zerowaste. Please check your connection and try again'),
            cancelText: gettextCatalog.getString('Cancel'),
            okText: gettextCatalog.getString('Retry')
          }).then(function(value) {
            // We get the dialog result as a boolean, so let's transform it to a resolved/rejected
            // promise.
            var deferred = $q.defer();
            if (value) {
              deferred.resolve();
            } else {
              deferred.reject();
            }

            return deferred.promise;
          });
        });

        return ezHelpers.retryRequestWhenPromiseIsResolved(popUpPromise, rejection);
      }

      return $q.reject(rejection);
    }
  };
})

/**
* Injects Accept-Language header to allow localized server responses.
*/
.factory('languageHeaderInterceptor', function($injector) {
  'use strict';

  return {
    'request': function(config) {
      var i18nService = $injector.get('i18nService');
      config.headers['Accept-Language'] = i18nService.getCurrentLanguage();

      return config;
    }
  };
})

/**
* Shows a loading overlay whenever an $http request is active.
*/
.factory('loadingInterceptor', function($q, $injector) {
  'use strict';

  function isEnabled(config) {
    return !config.skipLoadingInterceptor;
  }

  function hide(response) {
    if (!isEnabled(response.config)) {
      return;
    }

    $injector.get('chainedLoading').hide();
  }

  function show(config) {
    if (!isEnabled(config)) {
      return;
    }

    $injector.get('chainedLoading').show();
  }

  return {
    'request': function(config) {
      show(config);
      return config;
    },
    'response': function(response) {
      hide(response);
      return response;
    },
    'responseError': function(rejection) {
      hide(rejection);
      return $q.reject(rejection);
    }
  };
});
