angular.module('ezerowaste.services')

/**
* Encapsulates API calls to fetch ecopoints efficiently.
*/
.factory('ecopointsService', function(apiService, $q) {
  'use strict';
  var lastRequest = null;
  var lastResponse = null;

  function haversineDistance(a, b) {
    var radius = 6371000; // Earth radius (in meters)
    var deltaLatitude = toRadians(b.latitude - a.latitude);
    var deltaLongitude = toRadians(b.longitude - a.longitude);

    return 2 * radius * Math.asin(
      Math.sqrt(Math.pow(Math.sin(deltaLatitude / 2), 2) +
            Math.cos(toRadians(a.latitude)) * Math.cos(toRadians(b.latitude)) *
            Math.pow(Math.sin(deltaLongitude / 2), 2)));
  }

  function toRadians(value) {
    return value * (Math.PI / 180);
  }

  return {
    /**
     * Fetches the Ecopoints that are close to the provided location. This method keeps an internal
     * cache to avoid unnecessary requests.
     *
     * @param  {string} action The action used to filter Ecopoints (e.g 'Recycle').
     * @param  {Object} location The location to query Ecopoints.
     * @param  {number} location.latitude The latitude of the location.
     * @param  {number} location.longitude The longitude of the location.
     * @return {promise} A promise that will be resolved to the content of the /api/NearbyEcopoints
     *                   response, or the HTTP failure, if a request is performed and it fails.
     */
     getNearbyEcopoints: function(action, location, filteredBrand) {
       if (!lastRequest || lastRequest.action !== action || lastRequest.brand !== filteredBrand ||
           haversineDistance(location, lastRequest.location) > lastResponse.radiusInMeters) {
         var locationString = location.latitude + ',' + location.longitude;

         return apiService.get('NearbyEcopoints?actionType=' + action + '&location=' +
           locationString + (filteredBrand && filteredBrand.id != -1 ? '&filteredBrand=' + filteredBrand.id : ''))
         .then(function(response) {
           lastRequest = {
             location: angular.copy(location),
             action: action,
             brand: filteredBrand
           };
           lastResponse = response.data;

           return angular.copy(lastResponse);
         });
       }

       return $q.resolve(angular.copy(lastResponse));
     },
     /**
      * Fetches the Brands list.
      *
      * @return {promise} A promise that will be resolved to the content of the /api/admin/Brands
      *                   response, or the HTTP failure, if a request is performed and it fails.
      */
     getBrands: function() {
       return apiService.get('Brands');
     }
  };
});
