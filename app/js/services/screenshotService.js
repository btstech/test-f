angular.module('ezerowaste.services')

/**
* Takes screenshots of the application.
*/
.factory('screenshotService', function($window, $q) {
  'use strict';

  function getBase64ImageFromRasterize(drawFunction) {
    var canvas = angular.element('<canvas/>')[0];
    canvas.width = $window.screen.width;
    canvas.height = $window.screen.height;

    return $q.when(drawFunction(canvas).then(function(image) {
      return canvas.toDataURL();
    }));
  }

  var screenshotService = {
    /**
    * Takes a screenshot of the application
    * Returns a promise that is resolved with the base64 image.
    */
    takeScreenshot: function() {
      var deferred = $q.defer();

      $window.navigator.screenshot.URI(function(error, result) {
        if (error) {
          deferred.reject(error);
          return;
        }

        deferred.resolve(result.URI);
      });

      return deferred.promise;
    }
  };

  return screenshotService;
});
