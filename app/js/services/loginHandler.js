angular.module('ezerowaste.services')

/**
* Handles login requests.
*/
.factory('loginHandler', function($rootScope, gettextCatalog, dialogService, ezHelpers, $q,
  authService) {
  'use strict';
  var loginHandler = {
    /**
     * Attempts to login using the stored token.
     *
     * @return {promise} A promise such that:
     *                     - When the login request (but not necessarily the login itself) is
     *                       successful, it is resolved with a boolean indicating whether the login
     *                       was successful.
     *                     - When there is no refresh token available, it is resolved with false.
     *                     - When there is a problem while logging in, it is rejected with the
     *                       cause.
     */
    tryAutomaticLogin: function() {
      return authService.loginWithStoredToken().then(function() {
        return $q.resolve(true);
      }, function(response) {
        // Check if the refresh token is not valid anymore or we don't have one.
        if (!authService.refreshTokenExists() || response.status === 400) {
          return $q.resolve(false);
        }

        // Request error, reject with the same response.
        return $q.reject(response);
      });
    },

    /**
     * Attempts to login (requesting user credentials if necessary).
     *
     * @return {promise} A promise that is resolved when the login is sucessful, or rejected
     *                   otherwise.
     */
    requestLogin: function() {
      return ezHelpers.allowOneActivePromise('loginHandler.requestLogin', function() {
        return loginHandler.tryAutomaticLogin().then(function(loginSuccessful) {
          if (!loginSuccessful) {
            return dialogService.fromTemplateUrl('templates/login.html', {
              backdropClickToClose: false,
              hardwareBackButtonClose: false
            });
          }

          return $q.resolve();
        }).then(function() {
          var user = authService.currentUser;

          if (user.needsEmailConfirmation) {
            return dialogService.fromTemplateUrl('templates/confirm-email.html', {
              backdropClickToClose: false,
              hardwareBackButtonClose: false
            });
          }
        });
      });
    }
  };

  return loginHandler;
});
