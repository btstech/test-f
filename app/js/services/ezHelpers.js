angular.module('ezerowaste.services')

/**
* Common helpers.
*/
.factory('ezHelpers', function($http, $q) {
  'use strict';

  var activePromises = {};

  return {
    /**
    * Retries a request that failed with the provided 'rejection' reason when the provided promise
    * is resolved successfully.
    *
    * Returns a promise such that:
    *  - When the request is retried, is resolved with whatever outcome the new request has.
    *  - When the request is not retried, is rejected with the same reason the old request failed.
    */
    retryRequestWhenPromiseIsResolved: function(promise, rejection) {
      return promise.then(function() {
        return $http(rejection.config);
      }, function() {
        return $q.reject(rejection);
      });
    },

    /**
     * Allows at most one of the promises created by promiseFactory to run at the same time.
     * If there is no active promise with the given key, creates one using the provided factory, and
     * returns it. If there is an active promise with the same key, returns it (and promiseFactory
     * won't be called).
     *
     * @param  {string} key            The key used to denote this promises.
     * @param  {function} promiseFactory A function that creates the promise when no other promise
     *                                   with the same key is active.
     * @return {promise}               The active promise.
     */
    allowOneActivePromise: function(key, promiseFactory) {
      var oldPromise = activePromises[key];
      if (oldPromise != null) {
        return oldPromise;
      }

      activePromises[key] = promiseFactory().finally(function() {
        delete activePromises[key];
      });

      return activePromises[key];
    }
  };
});
