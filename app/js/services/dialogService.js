angular.module('ezerowaste.services')

/**
* Wraps Ionic dialogs in a promise
*/
.factory('dialogService', function($q, $rootScope, $ionicModal, $state) {
  'use strict';

  return {
    fromTemplateUrl: function(templateUrl, options) {
      var deferred = $q.defer();
      var newOptions = angular.extend({}, options);
      var scope;

      if (options.scope === undefined) {
        scope = newOptions.scope || $rootScope;
        scope = scope.$new();
        newOptions.scope = scope;
      } else {
        scope = newOptions.scope;
      }

      var modal;

      $ionicModal.fromTemplateUrl(templateUrl, newOptions).then(function(m) {
        modal = m;
        modal.show();
      });

      var resolved = false;
      scope.$on('modal.hidden', function() {
        if (!resolved) {
          resolved = true;
          modal.remove();
          deferred.reject();
        }
      });

      scope.close = function(result) {
        resolved = true;
        deferred.resolve(result);
        modal.hide().then(function() {
          modal.remove();
        });
      };

      scope.dismiss = function(result) {
        resolved = true;
        deferred.reject(result);
        modal.hide().then(function() {
          modal.remove();
        });
      };

      scope.isClosing = function() {
        return resolved;
      };

      return deferred.promise;
    }
  };
});
