angular.module('ezerowaste.services')

/**
* Attempts to reload the Google Maps Javascript API when there are network errors.
*/
.factory('mapsReloader', function($window, $document, $timeout) {
  'use strict';

  var lastScriptLoaded = false;
  var lastScriptWasError = false;

  // WORKAROUND: We don't have a way to detect failures the first time Maps is loaded, so we will
  // use a timeout. This attempts to prevent loading the Maps API multiple times.
  $timeout(function() {
    lastScriptLoaded = true;
    lastScriptWasError = !(angular.isDefined($window.google) &&
      angular.isDefined($window.google.maps));
  }, 3000);

  return {
    reload: function() {
      if (angular.isDefined($window.google) && angular.isDefined($window.google.maps)) {
        return;
      }

      if (!lastScriptLoaded || !lastScriptWasError) {
        return;
      }

      lastScriptLoaded = false;

      var element = _.find($document.find('script'),
        function(element) {
          return element.id && element.id.lastIndexOf('ui_gmap_map_load', 0) !== -1;
        });

      if (element) {
        var wrappedElement = angular.element(element);
        wrappedElement.remove();

        var newElement = $document[0].createElement('script');
        newElement.id = 'ui_gmap_map_load_' + Math.round(Math.random() * 1000);
        newElement.type = 'text/javascript';
        newElement.src = element.src;
        newElement.onload = function() {
          lastScriptLoaded = true;
          lastScriptWasError = false;
        };
        newElement.onerror = function() {
          lastScriptLoaded = true;
          lastScriptWasError = true;
        };
        $document[0].body.appendChild(newElement);
      }
    }
  };
});
