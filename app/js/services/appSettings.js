angular.module('ezerowaste.services')

/**
* Allows to read and save application settings
*/
.factory('appSettings', function(localStorageWrapper, i18nService) {
  'use strict';

  var result = {};

  /**
  * Creates a getter and setter for a property with the specified name.
  * The getter will return the defaultValue when there is no stored value (but it will not store
  * the default value). defaultValue can be either a function (in which case it will be called) or
  * any other value (in which case it will be returned directly).
  * Parameters customGet and customSet are optional, and replace the
  * generated getter and setter (which, by default, make use of getKey and
  * setKey). They receive the default getter/setter as the last parameter.
  */
  function defineSetting(name, defaultValue, customGet, customSet) {
    var defaultGet = function() {
      var result = localStorageWrapper.getKey(name);

      if (result === undefined) {
        result = angular.isFunction(defaultValue) ? defaultValue() : defaultValue;
      }

      return result;
    };

    var defaultSet = function(value) {
      localStorageWrapper.setKey(name, value);
    };

    var get = defaultGet;
    var set = defaultSet;

    if (customGet) {
      get = function() {
        return customGet(defaultGet);
      };
    }

    if (customSet) {
      set = function(value) {
        customSet(value, defaultSet);
      };
    }

    Object.defineProperty(result, name, {
      get: get,
      set: set
    });
  }

  // Default values
  defineSetting('tutorialShown', false);
  defineSetting('language', null, undefined, function(value, defaultSet) {
    defaultSet(value);
    i18nService.changeLanguage(value);
  });

  // Initial configuration
  i18nService.changeLanguage(result.language);

  return result;
});
