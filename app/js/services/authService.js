angular.module('ezerowaste.services')

/**
* Encapsulates authentication (login and logout) logic
*/
.factory('authService', function($http, tokenUrl, apiService, $q, $ionicPlatform,
  $cordovaDevice, localStorageWrapper, $window) {
  'use strict';

  function formEncode(data) {
    var str = [];
    Object.keys(data).forEach(function(key) {
      str.push(encodeURIComponent(key) + '=' + encodeURIComponent(data[key]));
    });
    return str.join('&');
  }

  var result;

  function performLogin(tokenRetriever) {
    return tokenRetriever().then(processTokenResponse);
  }

  function getDeviceIdPromise() {
    var deferred = $q.defer();
    $ionicPlatform.ready(function() {
      deferred.resolve($cordovaDevice.getUUID());
    });

    return deferred.promise;
  }

  function getRefreshToken() {
    return localStorageWrapper.getKey('refreshToken');
  }

  function processTokenResponse(response) {
    var tokenData = response.data;

    var accessToken = tokenData['access_token'];

    // Avoid leaving the token and user out of sync by manually inserting the access token for this
    // request.
    return apiService.get('Account/UserInfo', {
      headers: {
        'Authorization': 'Bearer ' + accessToken
      },
      skipAuthorizationHeaderInterceptor: true
    }).then(function(userResponse) {
      localStorageWrapper.setKey('refreshToken', tokenData['refresh_token']);
      result.token = accessToken;
      result.currentUser = userResponse.data;
    });
  }

  var activeRefreshTokenRequest = null;

  result = {

    /**
     * Stores the current access token
     */
    token: null,

    /**
     * Stores the current user object
     */
    currentUser: null,

    /**
     * Logins with the specified username and password

     * @return {Promise} A promise that resolves if the authentication is successful
     *                or is rejected otherwise
     */
    loginWithPassword: function(userName, password) {
      return performLogin(function() {
        return getDeviceIdPromise().then(function(id) {
          var credentials = {
            'grant_type': 'password',
            'client_id': id,
            userName: userName,
            password: password
          };

          return $http.post(tokenUrl, formEncode(credentials), {
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
            }
          });
        });
      });
    },

    /**
     * Logins using the Facebook Connect plugin
     */
    loginWithFacebook: function() {
      return performLogin(function() {
        var deferred = $q.defer();

        $ionicPlatform.ready(function() {
          $window.facebookConnectPlugin.login(['email'], deferred.resolve,
            deferred.reject);
        });

        return deferred.promise.then(function(facebookResponse) {
          return $http.post(tokenUrl, formEncode({
            'grant_type': 'facebook',
            'client_id': $cordovaDevice.getUUID(),
            token: facebookResponse.authResponse.accessToken
          }), {
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
            }
          });
        });
      });
    },

    /**
     * Returns true iff there is a refresh token stored.
     */
    refreshTokenExists: function() {
      return getRefreshToken() != null;
    },

    /**
     * Logins with the refresh token from previous requests (if available)
     *
     * @return {Promise} A promise that resolves if the authentication is successful
     *                or is rejected otherwise
     */
    loginWithStoredToken: function() {
      if (activeRefreshTokenRequest != null) {
        return activeRefreshTokenRequest;
      }

      var refreshToken = getRefreshToken();

      activeRefreshTokenRequest = performLogin(function() {
        if (!refreshToken) {
          return $q.reject('No refresh token available');
        }

        return getDeviceIdPromise().then(function(id) {
          var credentials = {
            'grant_type': 'refresh_token',
            'client_id': id,
            'refresh_token': refreshToken
          };

          return $http.post(tokenUrl, formEncode(credentials), {
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
            }
          });
        });
      }).finally(function() {
        activeRefreshTokenRequest = null;
      });

      return activeRefreshTokenRequest;
    },

    /**
     * Clears the token of the logged in user, and sends a request to the server to invalidate it.
     * Returns a promise that is resolved when the operation is completed.
     * This will not clear the local token if there is a problem with the server-side log out.
     */
    logout: function() {
      return apiService.post('Account/Logout').then(function() {
        result.token = result.currentUser = null;
        localStorageWrapper.setKey('refreshToken', null);
      });
    },

    /**
     * Returns true iff the user is logged in
     */
    isLoggedIn: function() {
      return result.token != null;
    }
  };

  return result;
});
