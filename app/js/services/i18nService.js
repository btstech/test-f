angular.module('ezerowaste.services')

/**
* Encapsulates language change logic
*/
.factory('i18nService', function(gettextCatalog, $ionicPlatform, $cordovaGlobalization) {
  'use strict';

  var currentLanguageCode = 'en';

  var i18nService = {
    /**
     * Changes the current language to the language with the provided languageCode. If languageCode
     * is null or undefined, it will set the language to the language of the device.
     */
    changeLanguage: function(languageCode) {
      if (languageCode == null) {
        $ionicPlatform.ready(function() {
          $cordovaGlobalization.getPreferredLanguage().then(function(preferredLanguage) {
            i18nService.changeLanguage(preferredLanguage.value);
          });
        });

        return;
      }

      languageCode = i18nService.getClosestLanguageCode(languageCode);
      currentLanguageCode = languageCode;
      if (languageCode.length > 2 && languageCode.charAt(2) === '-') {
        languageCode = languageCode.substr(0, 2) + '_' + languageCode.substr(3);
      }
      gettextCatalog.setCurrentLanguage(languageCode);
      if (gettextCatalog.baseLanguage !== languageCode) {
        gettextCatalog.loadRemote('translations/' + languageCode + '.json');
      }
    },

    /**
     * Returns a list of the available languages. Each element of the returned array is an object
     * with the code and name.
     */
    getAvailableLanguages: function() {
      return [
        {
          code: 'en',
          name: 'English'
        },
        {
          code: 'es-CO',
          name: 'Español'
        },
        {
          code: 'pt-BR',
          name: 'Português'
        }
      ];
    },

    /**
     * Returns the language code that is closest to the provided code, in the available languages.
     * It will falllback to English if no other language matches the given code.
     */
    getClosestLanguageCode: function(languageCode) {
      var result = 'en';

      function normalizedCode(languageCode) {
        return languageCode.length > 2 ? languageCode.substr(0, 2) : languageCode;
      }

      var normalizedTargetCode = normalizedCode(languageCode);

      var availableLanguages = i18nService.getAvailableLanguages();
      for (var i = 0; i < availableLanguages.length; ++i) {
        var language = availableLanguages[i];

        if (language.code === languageCode) {
          // Exact match, return directly
          return languageCode;
        }

        if (normalizedTargetCode === normalizedCode(language.code)) {
          // If we already chose the normalized language, keep it. Otherwise, keep this as the
          // best match so far.
          if (result !== normalizedTargetCode) {
            result = language.code;
          }
        }
      }

      return result;
    },

    /**
     * Returns the current language. The difference between this function and directly calling
     * appSettings.language is that this function will return a language code even if the language
     * is the device language.
     */
    getCurrentLanguage: function() {
      return currentLanguageCode;
    }
  };

  return i18nService;
});
