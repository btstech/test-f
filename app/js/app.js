angular.module('ezerowaste',
               ['ionic', 'ezerowaste.controllers', 'ezerowaste.services',
                 'ngCordova', 'gettext', 'ngMessages', 'uiGmapgoogle-maps', 'ngSanitize'])

.config(function(uiGmapGoogleMapApiProvider, $compileProvider) {
  'use strict';

  uiGmapGoogleMapApiProvider.configure({
    v: '3.22',
    libraries: ['places']
  });

  $compileProvider.aHrefSanitizationWhitelist(
    /^\s*(https?|ftp|mailto|tel|file|maps|geo):/);
})

.run(function($ionicPlatform, $rootScope, gettextCatalog, $window) {
  'use strict';

  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if ($window.cordova && $window.cordova.plugins && $window.cordova.plugins.Keyboard) {
      $window.cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if ($window.StatusBar) {
      // org.apache.cordova.statusbar required
      $window.StatusBar.styleDefault();
    }
  });

  // WORKAROUND: Because Ionic caches some views, the angular-gettext language
  // change event is not broadcasted to inactive views, and we may get
  // stale translations. Let's workaround it by storing the language that each
  // scope has and broadcast the event manually when it changes.
  var lastKnownLanguage = {};
  $rootScope.$on('$ionicView.beforeEnter', function(event) {
    var id = event.targetScope.$id;
    var lastKnown = lastKnownLanguage[id];
    if (lastKnown !== gettextCatalog.getCurrentLanguage()) {
      event.targetScope.$broadcast('gettextLanguageChanged');
    }
  });
  $rootScope.$on('$ionicView.leave', function(event) {
    lastKnownLanguage[event.targetScope.$id] = gettextCatalog.getCurrentLanguage();
  });
})

.config(function($stateProvider, $urlRouterProvider, $httpProvider) {
  'use strict';

  $stateProvider
  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppController'
  })
  .state('tutorial', {
    url: '/tutorial',
    templateUrl: 'templates/tutorial.html',
    controller: 'TutorialController'
  })
  .state('app.main', {
    url: '/main',
    views: {
      'menuContent': {
        templateUrl: 'templates/main.html',
        controller: 'MainController'
      }
    }
  })
  .state('app.processDevice', {
    abstract: true,
    url: '/processDevice',
    views: {
      'menuContent': {
        template: '<ion-nav-view/>',
        controller: 'ProcessDeviceController'
      }
    }
  })
  .state('app.processDevice.1-SelectDevice', {
    url: '', // Make this the default state when going to the parent URL
    templateUrl: 'templates/process-device/1-select-device.html'
  })
  .state('app.processDevice.2-SelectStatus', {
    templateUrl: 'templates/process-device/2-select-status.html'
  })
  .state('app.processDevice.3-SelectAction', {
    templateUrl: 'templates/process-device/3-select-action.html'
  })
  .state('app.processDevice.4-Result', {
    templateUrl: 'templates/process-device/4-result.html'
  })
  .state('app.processDevice.5-Statistics', {
    templateUrl: 'templates/process-device/5-statistics.html'
  })
  .state('app.news', {
    url: '/news',
    views: {
      'menuContent': {
        templateUrl: 'templates/news.html',
        controller: 'NewsController'
      }
    }
  })
  .state('app.history', {
    url: '/history',
    views: {
      'menuContent': {
        templateUrl: 'templates/history.html',
        controller: 'HistoryController'
      }
    }
  })
  .state('app.statistics', {
    url: '/statistics',
    views: {
      'menuContent': {
        templateUrl: 'templates/statistics.html',
        controller: 'StatisticsController'
      }
    }
  })
  .state('app.settings', {
    url: '/settings',
    views: {
      'menuContent': {
        templateUrl: 'templates/settings.html',
        controller: 'SettingsController'
      }
    }
  })
  .state('app.about', {
    url: '/about',
    views: {
      'menuContent': {
        templateUrl: 'templates/about.html',
        controller: 'AboutController'
      }
    }
  });

  $urlRouterProvider.otherwise('/app/main');

  // HTTP Header interception.
  // Keep in mind that AngularJS calls these interceptors in order for requests and in reverse order
  // for responses.

  // Request interceptors:
  $httpProvider.interceptors.push('languageHeaderInterceptor');
  $httpProvider.interceptors.push('authorizationHeaderInterceptor');

  // Response interceptors (again, they will be called in reverse order):
  $httpProvider.interceptors.push('unauthorizedInterceptor');
  $httpProvider.interceptors.push('unreachableInterceptor');
  $httpProvider.interceptors.push('loadingInterceptor');

  $httpProvider.interceptors.push(function() {
    return {
      'request': function(config) {
        if (config.timeout == null) {
          config.timeout = 10000;
        }
        return config;
      }
    };
  });
})
.constant('apiUrl', 'https://ezw.azurewebsites.net/api/')
.constant('tokenUrl', 'https://ezw.azurewebsites.net/Token')
/*.constant('apiUrl', 'https://192.168.0.20:44300/api/')
.constant('tokenUrl', 'https://192.168.0.20:44300/Token')*/
.constant('appInfo', {
  version: '1.0'
});

angular.module('ezerowaste.controllers', ['ezerowaste.services']);
angular.module('ezerowaste.services', ['ionic', 'ngCordova']);
