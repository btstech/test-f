angular.module('ezerowaste')

.filter('mapsLink', function($cordovaDevice) {
  'use strict';

  return function(location) {
    if (!location) {
      return '';
    }

    var locationString = location.latitude + ',' + location.longitude;
    switch ($cordovaDevice.getPlatform()) {
      case 'Android': return 'geo:0,0?q=' + locationString;
      case 'iOS': return 'maps://?q=' + locationString;
      default: throw 'Unsupported platform';
    }
  };
});
