angular.module('ezerowaste')

.filter('argbIntToCssColor', function() {
  'use strict';

  return function(decimalArgb) {
    // Convert any string to integer first (or otherwise the next addition will be concatenation).
    decimalArgb = parseInt(decimalArgb, 10);

    if (decimalArgb < 0) {
      // Javascript uses 64-bit floating point numbers, but the color is a 32-bit signed integer.
      // Transform it to its equivalent unsigned 32-bit integer to proceed.
      // 1 << n for n >= 31 will overflow, so we force float arithmetic
      decimalArgb += 4 * (1 << 30);
    }

    var bytes = new Array(4);
    for (var i = 0; i < 4; ++i) {
      var currentByte = decimalArgb >> (8 * (3 - i)) & 0xFF;
      // Transform ARGB to RGBA
      bytes[(i + 3) % 4] = currentByte;
    }

    bytes[3] /= 255;

    return 'rgba(' + bytes.join(', ') + ')';
  };
});
