'use strict';

describe('Filter: argbIntToCssColor', function() {
  beforeEach(function() {
    module('ezerowaste');
  });

  var $filter, argbIntToCssColor;
  beforeEach(function() {
    inject(function(_$filter_) {
      $filter = _$filter_;
      argbIntToCssColor = $filter('argbIntToCssColor');
    });
  });

  it('converts an integer ARGB color to a CSS string', function() {
    var tests = [
      [-1, 'rgba(255, 255, 255, 1)'], [0, 'rgba(0, 0, 0, 0)'],
      [-16777216, 'rgba(0, 0, 0, 1)'],
      [-144358623, 'rgba(101, 67, 33, ' + (247 / 255) + ')'],
      [123456789, 'rgba(91, 205, 21, ' + (7 / 255) + ')'],
      [0xFF5B3B18, 'rgba(91, 59, 24, 1)']
    ];

    for (var i = 0; i < tests.length; ++i) {
      expect(argbIntToCssColor(tests[i][0])).toBe(tests[i][1]);
    }
  });
});
