'use strict';

describe('Service: localStorageWrapper', function() {
  beforeEach(module('ezerowaste.services'));

  var localStorageWrapper, localStorage;

  beforeEach(function() {
    inject(function(_localStorageWrapper_, _$window_) {
      localStorageWrapper = _localStorageWrapper_;
      localStorage = _$window_.localStorage;
    });
  });

  it('should write JSON to localStorage', function() {
    var keyArgument, valueArgument;
    spyOn(localStorage, 'setItem').and.callFake(function(key, value) {
      keyArgument = key;
      valueArgument = value;
    });

    var input = ['a string', true, 1, {a: 'something'}];

    for (var i = 0; i < input.length; ++i) {
      localStorageWrapper.setKey('setting key', input[i]);

      expect(localStorage.setItem).toHaveBeenCalled();
      expect(keyArgument).toBe('setting key');
      expect(angular.fromJson(valueArgument)).toEqual(input[i]);
    }
  });

  it('should read a JSON string from localStorage', function() {
    var value;
    spyOn(localStorage, 'getItem').and.callFake(function(key) {
      return value;
    });

    var input = ['a string', true, 1, {a: 'something'}];
    for (var i = 0; i < input.length; ++i) {
      value = angular.toJson(input[i]);
      var result = localStorage.getItem('setting key');

      expect(localStorage.getItem).toHaveBeenCalledWith('setting key');
      expect(angular.fromJson(result)).toEqual(input[i]);
    }
  });
});
