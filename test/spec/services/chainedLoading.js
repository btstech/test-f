'use strict';

describe('Service: chainedLoading', function() {
  beforeEach(function() {
    module('ezerowaste.services');
    module('ionic');
  });

  var chainedLoading, $ionicLoading;
  beforeEach(function() {
    inject(function(_chainedLoading_, _$ionicLoading_) {
      chainedLoading = _chainedLoading_;
      $ionicLoading = _$ionicLoading_;
    });
  });

  it('should send the options to $ionicLoading', function() {
    spyOn($ionicLoading, 'show');

    var options = {'asd': 'sdf'};
    chainedLoading.show(options);

    expect($ionicLoading.show).toHaveBeenCalledWith(options);
  });

  it('should call $ionicLoading.show() as many times as chainedLoading.show()',
  function() {
    spyOn($ionicLoading, 'show');

    chainedLoading.show();
    chainedLoading.show();

    expect($ionicLoading.show.calls.count()).toBe(2);
  });

  it('should be marked as isShown whenever is shown', function() {
    expect(chainedLoading.isShown()).toBe(false);
    chainedLoading.show();
    expect(chainedLoading.isShown()).toBe(true);
    chainedLoading.hide();
    expect(chainedLoading.isShown()).toBe(false);
  });

  it('should ignore additional hide() calls', function() {
    expect(chainedLoading.isShown()).toBe(false);
    chainedLoading.hide();
    expect(chainedLoading.isShown()).toBe(false);
    chainedLoading.show();
    expect(chainedLoading.isShown()).toBe(true);
  });

  it('should call $ionicLoading.hide() only if it is the last show/hide pair',
  function() {
    spyOn($ionicLoading, 'hide');
    spyOn($ionicLoading, 'show');

    chainedLoading.show();
    chainedLoading.show();
    chainedLoading.hide();

    expect($ionicLoading.hide.calls.count()).toBe(0);

    chainedLoading.hide();
    expect($ionicLoading.hide.calls.count()).toBe(1);
  });
});
