'use strict';

describe('authService', function() {
  beforeEach(function() {
    module('ezerowaste.services');
    module(function($provide) {
      $provide.constant('apiUrl', 'http://server.test.com/');
      $provide.constant('tokenUrl', 'http://server.test.com/token');

      $provide.factory('$cordovaDevice', function() {
        return {
          getUUID: function() {
            return 'theClient';
          }
        };
      });

      $provide.factory('localStorageWrapper', function() {
        var data = {};
        return {
          getKey: function(key) {
            return data[key];
          },
          setKey: function(key, value) {
            data[key] = value;
          }
        };
      });
    });
  });

  var authService, $httpBackend, tokenUrl, apiUrl, $window, $ionicPlatform, localStorageWrapper,
      $rootScope;

  beforeEach(function() {
    inject(function(_authService_, _$httpBackend_, _tokenUrl_, _apiUrl_, _$window_,
      _$ionicPlatform_, _localStorageWrapper_, _$rootScope_) {
      authService = _authService_;
      $httpBackend = _$httpBackend_;
      tokenUrl = _tokenUrl_;
      apiUrl = _apiUrl_;
      $window = _$window_;
      $ionicPlatform = _$ionicPlatform_;
      localStorageWrapper = _localStorageWrapper_;
      $rootScope = _$rootScope_;

      // Prevent template requests caused by prefetching
      $httpBackend.whenGET(/templates\/.*$/).respond('');

      // authService works after deviceReady
      var ready = spyOn($ionicPlatform, 'ready');

      ready.and.callFake(function(callback) {
        callback();
      });
    });
  });

  afterEach(function() {
    $httpBackend.verifyNoOutstandingExpectation();
  });

  function testLoginMethod(expectedData, loginFunction) {
    $httpBackend.expectPOST(tokenUrl, expectedData)
    .respond(200, {
      'access_token': 'the_token',
      'refresh_token': 'the_refresh_token'
    });

    var realUser = {'something': 'good'};
    $httpBackend.expectGET(apiUrl + 'Account/UserInfo')
    .respond(200, realUser);

    var then = jasmine.createSpy('then');

    loginFunction().then(then);

    $httpBackend.flush();

    expect(authService.token).toBe('the_token');
    expect(localStorageWrapper.getKey('refreshToken')).toBe('the_refresh_token');
    expect(authService.currentUser).toEqual(realUser);
    expect(authService.isLoggedIn()).toBe(true);
    expect(then).toHaveBeenCalled();
  }

  it('should login with user name and password', function() {
    testLoginMethod(
      'grant_type=password&client_id=theClient&userName=user1&password=pass', function() {
        return authService.loginWithPassword('user1', 'pass');
      }
    );
  });

  it('should login with Facebook', function() {
    var fbPlugin = $window.facebookConnectPlugin =
      jasmine.createSpyObj('facebookConnectPlugin', ['login']);

    fbPlugin.login.and.callFake(function(permissions, successCallback,
      failCallback) {
      successCallback({
        authResponse: {
          accessToken: 'a_token'
        }
      });
    });

    testLoginMethod('grant_type=facebook&client_id=theClient&token=a_token', function() {
      return authService.loginWithFacebook();
    });
  });

  describe('loginWithStoredToken', function() {
    it('should login with the stored refresh token', function() {
      localStorageWrapper.setKey('refreshToken', 'the_refresh_token');

      testLoginMethod(
        'grant_type=refresh_token&client_id=theClient&refresh_token=the_refresh_token', function() {
          return authService.loginWithStoredToken();
        }
      );
    });

    it('should fail without sending a request when there is no refresh token', function() {
      var fail = jasmine.createSpy('fail');
      authService.loginWithStoredToken().then(undefined, fail);
      $rootScope.$digest();

      expect(fail).toHaveBeenCalled();

      $httpBackend.verifyNoOutstandingExpectation();
    });

    it('should not reuse an invalidated refresh token when simultaneous calls are made',
    function() {
      localStorageWrapper.setKey('refreshToken', 'token_1');
      $httpBackend.expectPOST(tokenUrl,
        'grant_type=refresh_token&client_id=theClient&refresh_token=token_1')
      .respond(200, {
        'access_token': 'the_token',
        'refresh_token': 'token_2'
      });

      // The implementation is not required but is allowed to request a second token (it can merge
      // simultaneous requests in only one). The fact that we are using expect() above makes it
      // only work once (so it won't allow the same token to be used twice).
      $httpBackend.whenPOST(tokenUrl,
        'grant_type=refresh_token&client_id=theClient&refresh_token=token_2')
      .respond(200, {
        'access_token': 'the_token',
        'refresh_token': 'token_3'
      });

      $httpBackend.whenPOST(tokenUrl)
      .respond(401, {});

      var realUser = {'something': 'good'};
      $httpBackend.whenGET(apiUrl + 'Account/UserInfo').respond(200, realUser);

      var then = jasmine.createSpy('then');
      // Two simultaneous requests. It is essential to ensure that both promises are resolved to
      // guarantee that we don't reach the 401 response.
      authService.loginWithStoredToken().then(then);
      authService.loginWithStoredToken().then(then);

      $httpBackend.flush();
      $httpBackend.verifyNoOutstandingExpectation();

      expect(then.calls.count()).toBe(2);
    });
  });

  it('should clear auth data and call Account/Logout when logging out', function() {
    authService.token = 'something';
    authService.currentUser = {'a': 'b'};
    localStorageWrapper.setKey('refreshToken', 'the_refresh_token');

    $httpBackend.expectPOST(apiUrl + 'Account/Logout')
    .respond(200);

    var then = jasmine.createSpy('then');

    authService.logout().then(then);

    $httpBackend.flush();

    expect(authService.token).toBe(null);
    expect(authService.currentUser).toBe(null);
    expect(localStorageWrapper.getKey('refreshToken')).toBe(null);
    expect(then).toHaveBeenCalled();
  });
});
