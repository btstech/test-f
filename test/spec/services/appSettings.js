'use strict';

describe('Service: appSettings', function() {
  beforeEach(function() {
    module('ezerowaste.services');
    module(function($provide) {
      $provide.factory('i18nService', function() {
        return {
          changeLanguage: function() {

          }
        };
      });

      $provide.factory('$cordovaGlobalization', function($q) {
        return {
          getPreferredLanguage: function() {
            return $q.resolve('en-US');
          }
        };
      });
    });
  });

  var appSettings, localStorageWrapper;
  beforeEach(function() {
    inject(function(_appSettings_, _localStorageWrapper_) {
      appSettings = _appSettings_;
      localStorageWrapper = _localStorageWrapper_;
    });
  });

  it('should write to the provided storage', function() {
    spyOn(localStorageWrapper, 'setKey');
    appSettings.language = 'dummy';
    expect(localStorageWrapper.setKey)
      .toHaveBeenCalledWith('language', 'dummy');
  });

  it('should read from the provided storage', function() {
    spyOn(localStorageWrapper, 'getKey').and.callFake(function(key) {
      return key === 'language' ? 'pass' : 'fail';
    });

    var result = appSettings.language;
    expect(result).toBe('pass');
    expect(localStorageWrapper.getKey).toHaveBeenCalledWith('language');
  });
});
