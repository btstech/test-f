'use strict';

describe('Service: loadingInterceptor', function() {
  beforeEach(function() {
    module(function($httpProvider) {
      $httpProvider.interceptors.push('loadingInterceptor');
    });
    module('ezerowaste.services');
    module('ionic');
  });

  var chainedLoading, $httpBackend, $http, loadingInterceptor;
  beforeEach(function() {
    inject(function(_chainedLoading_, _$httpBackend_, _$http_, _loadingInterceptor_) {
      chainedLoading = _chainedLoading_;
      $httpBackend = _$httpBackend_;
      $http = _$http_;
      loadingInterceptor = _loadingInterceptor_;
    });
  });

  afterEach(function() {
     $httpBackend.verifyNoOutstandingExpectation();
   });

  it('should show the loading overlay while a request is active', function() {
    var targetUrl = 'http://something.com';
    var statusList = [200, 400];
    spyOn(chainedLoading, 'show');
    spyOn(chainedLoading, 'hide');

    for (var i = 0; i < statusList.length; ++i) {
      var status = statusList[i];

      $httpBackend.whenGET(targetUrl).respond(status, '');

      // We need to send two requests because the mock doesn't call the request interceptor until
      // the first flush
      $http.get(targetUrl);
      $http.get(targetUrl);

      $httpBackend.flush(1);

      expect(chainedLoading.show.calls.count()).toBe(2);
      expect(chainedLoading.hide.calls.count()).toBe(1);

      $httpBackend.flush(1);
      expect(chainedLoading.hide.calls.count()).toBe(2);

      chainedLoading.show.calls.reset();
      chainedLoading.hide.calls.reset();
    }
  });

  it('can be skipped using the skipLoadingInterceptor config', function() {
    expect(chainedLoading.isShown()).toBe(false);

    var targetUrl = 'http://something.com';
    spyOn(chainedLoading, 'show');
    spyOn(chainedLoading, 'hide');

    $httpBackend.whenGET(targetUrl).respond(200, '');

    $http.get(targetUrl, {
      skipLoadingInterceptor: true
    });

    $httpBackend.flush();

    expect(chainedLoading.show).not.toHaveBeenCalled();
    expect(chainedLoading.hide).not.toHaveBeenCalled();
  });
});

describe('Service: unauthorizedInterceptor', function() {
  beforeEach(function() {
    module(function($httpProvider) {
      $httpProvider.interceptors.push('unauthorizedInterceptor');
    });
    module('ezerowaste.services');
    module(function($provide) {
      $provide.constant('apiUrl', 'something.com/api/');
      $provide.factory('loginHandler', function() {
        return jasmine.createSpyObj('loginHandler', ['requestLogin']);
      });
    });
  });

  var $httpBackend, $http, $q, loginHandler, apiUrl;
  beforeEach(function() {
    inject(function(_$httpBackend_, _$http_, _$q_, _loginHandler_, _apiUrl_) {
      $httpBackend = _$httpBackend_;
      $http = _$http_;
      $q = _$q_;
      loginHandler = _loginHandler_;
      apiUrl = _apiUrl_;
    });
  });

  afterEach(function() {
     $httpBackend.verifyNoOutstandingExpectation();
   });

  it('should retry API requests after receiving 401 and successful login',
  function() {
    var targetUrl = apiUrl;

    $httpBackend.expectGET(targetUrl).respond(401, '');

    loginHandler.requestLogin.and.callFake(function() {
      $httpBackend.expectGET(targetUrl).respond(200, 'real response');
      return $q.resolve();
    });

    $http.get(targetUrl);

    $httpBackend.flush();

    expect(loginHandler.requestLogin).toHaveBeenCalled();
  });

  it('should not request login for non-API requests',
  function() {
    var targetUrl = 'non-api-url.com';

    $httpBackend.expectGET(targetUrl).respond(401, '');

    $http.get(targetUrl);

    $httpBackend.flush();

    expect(loginHandler.requestLogin).not.toHaveBeenCalled();
  });

  it('should fail API requests after receiving 401 and unsuccessful login',
  function() {
    var targetUrl = apiUrl;

    $httpBackend.expectGET(targetUrl).respond(401, '');

    loginHandler.requestLogin.and.callFake(function() {
      return $q.reject();
    });

    var error = jasmine.createSpy('error');
    $http.get(targetUrl).then(undefined, error);

    $httpBackend.flush();
    expect(loginHandler.requestLogin).toHaveBeenCalled();
    expect(error).toHaveBeenCalled();
  });
});

describe('Service: authorizationHeaderInterceptor', function() {
  beforeEach(function() {
    module(function($httpProvider) {
      $httpProvider.interceptors.push('authorizationHeaderInterceptor');
    });
    module('ezerowaste.services');
    module(function($provide) {
      $provide.constant('apiUrl', 'something.com/api/');
      $provide.factory('authService', function($q) {
        var result = {
          token: null
        };

        return result;
      });
    });
  });

  var authService, $httpBackend, $http, $q, apiUrl;
  beforeEach(function() {
    inject(function(_authService_, _$httpBackend_, _$http_, _$q_, _apiUrl_) {
      authService = _authService_;
      $httpBackend = _$httpBackend_;
      $http = _$http_;
      apiUrl = _apiUrl_;
      $q = _$q_;
    });
  });

  afterEach(function() {
     $httpBackend.verifyNoOutstandingExpectation();
   });

  it('can be skipped using the skipAuthorizationHeaderInterceptor config', function() {
    var token = authService.token = 'a_token';

    $httpBackend.expectGET(apiUrl, function(headers) {
      return headers['Authorization'] == null;
    }).respond(200, '');

    $http.get(apiUrl, {
      skipAuthorizationHeaderInterceptor: true
    });
    $httpBackend.flush();
  });

  it('should inject the token in the Authorization header to API calls',
  function() {
    var token = authService.token = 'a_token';

    $httpBackend.expectGET(apiUrl, function(headers) {
      return headers['Authorization'] === 'Bearer ' + token;
    }).respond(200, '');

    $http.get(apiUrl);
    $httpBackend.flush();
  });

  it('should not inject the token on non-API calls',
  function() {
    var targetUrl = 'http://nonapi.com';
    var token = authService.token = 'a_token';

    $httpBackend.expectGET(targetUrl,function(headers) {
      return headers['Authorization'] == null;
    }).respond(200, '');

    $http.get(targetUrl);
    $httpBackend.flush();
  });
});

describe('Service: languageHeaderInterceptor', function() {
  beforeEach(function() {
    module('ezerowaste.services');
    module(function($provide) {
      $provide.factory('i18nService', function() {
        return jasmine.createSpyObj('i18nService', ['getCurrentLanguage']);
      });
    });

    module(function($httpProvider) {
      $httpProvider.interceptors.push('languageHeaderInterceptor');
    });
  });

  var i18nService, $http, $httpBackend;

  beforeEach(function() {
    inject(function(_i18nService_, _$http_, _$httpBackend_) {
      i18nService = _i18nService_;
      $http = _$http_;
      $httpBackend = _$httpBackend_;
    });
  });

  afterEach(function() {
     $httpBackend.verifyNoOutstandingExpectation();
   });

  it('should add the current language to Accept-Language header when performing' +
    'API requests (GET, POST, PUT, DELETE)', function() {
    var targetUrl = 'http://localhost/';

    var methods = ['GET', 'POST', 'PUT', 'DELETE'];
    var languages = ['en', 'es-CO', 'pt-BR'];

    function checkHeaders(headers) {
      return headers['Accept-Language'] === language;
    }

    for (var i = 0; i < languages.length; ++i) {
      var language = languages[i];
      i18nService.getCurrentLanguage.and.returnValue(language);
      for (var j = 0; j < methods.length; ++j) {
        var method = methods[j];
        $httpBackend.expect(method, targetUrl, undefined, checkHeaders).respond(200, '');

        $http({
          method: method,
          url: targetUrl,
          data: ''
        });
      }
    }

    $httpBackend.flush();
  });
});

describe('Service: unreachableInterceptor', function() {
  beforeEach(function() {
    module('gettext');
    module(function($httpProvider) {
      $httpProvider.interceptors.push('unreachableInterceptor');
    });
    module('ezerowaste.services');
    module(function($provide) {
      $provide.factory('$ionicPopup', function() {
        return jasmine.createSpyObj('$ionicPopup', ['confirm']);
      });
    });
  });

  var $httpBackend, $http, $q, $ionicPopup;
  beforeEach(function() {
    inject(function(_$httpBackend_, _$http_, _$q_, _$ionicPopup_) {
      $httpBackend = _$httpBackend_;
      $http = _$http_;
      $ionicPopup = _$ionicPopup_;
      $q = _$q_;
    });
  });

  afterEach(function() {
     $httpBackend.verifyNoOutstandingExpectation();
   });

  var endpoint = 'http://test.com';

  it('should show a popup for unreachable requests', function() {
    // Angular normalizes codes < -1 to 0, but -1 is a special case. Still, we test some negative
    // values in case angular changes this behavior
    var unreachableCodes = [-100, -2, -1, 0];

    for (var i = 0; i < unreachableCodes.length; ++i) {
      $httpBackend.expectGET(endpoint).respond(unreachableCodes[i], '');
      $ionicPopup.confirm.and.returnValue($q.resolve(false));

      $http.get(endpoint);
      $httpBackend.flush(1);

      if ($ionicPopup.confirm.calls.count() !== 1) {
        fail('Expected to get a popup for status ' + unreachableCodes[i]);
      }
      $ionicPopup.confirm.calls.reset();
    }
  });

  it('should retry the request when the user answers yes to the popup',
  function() {
    $httpBackend.expectGET(endpoint).respond(0, '');
    $ionicPopup.confirm.and.callFake(function() {
      $httpBackend.expectGET(endpoint).respond(200, '');
      return $q.resolve(true);
    });

    $http.get(endpoint);

    $httpBackend.flush();
    expect($ionicPopup.confirm).toHaveBeenCalled();
  });

  it('should not retry the request when the user answers no to the popup',
  function() {
    $httpBackend.expectGET(endpoint).respond(0, '');
    $ionicPopup.confirm.and.returnValue($q.resolve(false));

    $http.get(endpoint);

    $httpBackend.flush();
  });

  it('should not handle success, server-error or request-error codes',
  function() {
    var codes = [200, 400, 500];

    for (var i = 0; i < codes.length; ++i) {
      $httpBackend.expectGET(endpoint).respond(codes[i], '');
      $http.get(endpoint);
    }

    $httpBackend.flush();
    expect($ionicPopup.confirm).not.toHaveBeenCalled();
  });
});
