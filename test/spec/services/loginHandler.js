'use strict';

describe('Service: loginHandler', function() {
  beforeEach(function() {
    module('ionic');
    module('gettext');
    module('ezerowaste.services');
    module(function($provide) {
      $provide.factory('dialogService', function() {
        return jasmine.createSpyObj('dialogService', ['fromTemplateUrl']);
      });

      $provide.factory('authService', function() {
        return jasmine.createSpyObj('authService', ['loginWithStoredToken', 'refreshTokenExists']);
      });
    });
  });

  var loginHandler, dialogService, $q, $rootScope, authService;

  beforeEach(function() {
    inject(function(_loginHandler_, _dialogService_, _$q_, _$rootScope_, _authService_,
    _$httpBackend_) {
      loginHandler = _loginHandler_;
      dialogService = _dialogService_;
      $q = _$q_;
      $rootScope = _$rootScope_;
      authService = _authService_;

      // Ionic tries to prefetch the templates, so we can get unexpected
      // requests and we should handle them or the tests may fail
      _$httpBackend_.whenGET(/^templates\/.*/).respond('');
    });
  });

  it('should open a dialog and return its promise when login is requested and no automatic login ' +
    'is possible',
  function() {
    authService.loginWithStoredToken.and.returnValue($q.reject());
    authService.refreshTokenExists.and.returnValue(false);
    authService.currentUser = {
      emailConfirmed: true
    };

    dialogService.fromTemplateUrl.and.returnValue($q.resolve());
    var then = jasmine.createSpy('then');

    loginHandler.requestLogin().then(then);
    $rootScope.$digest();
    expect(dialogService.fromTemplateUrl).toHaveBeenCalledWith(
      'templates/login.html', jasmine.any(Object));
    expect(then).toHaveBeenCalled();

    var error = jasmine.createSpy('error');
    dialogService.fromTemplateUrl.and.returnValue($q.reject());
    loginHandler.requestLogin().then(undefined, error);
    $rootScope.$digest();
    expect(error).toHaveBeenCalled();

    expect(authService.loginWithStoredToken.calls.count()).toEqual(2);
  });

  it('should open exactly one dialog when login is requested before a previous request ends',
  function() {
    authService.loginWithStoredToken.and.returnValue($q.reject());
    authService.refreshTokenExists.and.returnValue(false);
    var deferred = $q.defer();

    dialogService.fromTemplateUrl.and.returnValue(deferred.promise);

    var promise = loginHandler.requestLogin();
    var promise2 = loginHandler.requestLogin();

    $rootScope.$digest();
    expect(dialogService.fromTemplateUrl.calls.count()).toBe(1);
    expect(promise).toBe(promise2);
  });

  it('should not open a dialog and resolve the promise if automatic login is possible',
  function() {
    authService.loginWithStoredToken.and.returnValue($q.resolve());
    authService.currentUser = {
      emailConfirmed: true
    };

    var then = jasmine.createSpy('then');
    loginHandler.requestLogin().then(then);

    $rootScope.$digest();

    expect(then).toHaveBeenCalled();
    expect(authService.loginWithStoredToken).toHaveBeenCalled();
    expect(dialogService.fromTemplateUrl).not.toHaveBeenCalled();
  });
});
