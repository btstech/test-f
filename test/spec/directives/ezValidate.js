'use strict';

describe('Directive: ezValidate', function() {
  beforeEach(function() {
    module('ezerowaste');
  });

  var $compile, $rootScope, $templateCache, $httpBackend, element;
  beforeEach(function() {
    inject(function(_$compile_, _$rootScope_, _$templateCache_,
                    _$httpBackend_) {
      $compile = _$compile_;
      $rootScope = _$rootScope_;
      $templateCache = _$templateCache_;
      $httpBackend = _$httpBackend_;

      // Manually set up the response for the required template
      // We set to an empty div instead of an empty string to workaround
      // https://github.com/angular/angular.js/issues/12941
      $templateCache.put('templates/error-messages.html', '<div></div>');

      // Avoid errors for other requests
      $httpBackend.whenGET(/templates\/.*$/).respond('');
    });
  });

  function createContainedElement(scope, attributes) {
    var container = angular.element('<div/>');
    var form = angular.element('<form name="form"></form>');
    var element = angular.element(
      '<div ez-validate="form.model"></div>');
    element.attr(attributes || {});

    form.append('<input type="text" ng-model="model" name="model" required>');
    form.append(element);

    container.append(form);
    $compile(container)(scope);

    return container;
  }

  it('shows server errors', function() {
    var errors;
    $rootScope.errors = errors = ['an error from the server'];

    var container = createContainedElement($rootScope, {
      'ez-validate': '',
      'server-errors': 'errors'
    });
    $rootScope.$digest();

    for (var i = 0; i < errors.length; ++i) {
      expect(container.html()).toContain(errors[i]);
    }
  });

  it('should not show client-side errors when there are server-side errors',
  function() {
    $templateCache.put('templates/error-messages.html',
      '<div ng-message="required">Field is required</div>');

    $rootScope.errors = ['an error from the server'];

    var container = createContainedElement($rootScope, {
      'server-errors': 'errors'
    });
    $rootScope.form.$setSubmitted();
    $rootScope.$digest();

    expect(container.html()).not.toContain('Field is required');
  });

  it('shows form errors from the default template when submitted or dirty',
  function() {
    $templateCache.put('templates/error-messages.html',
      '<div ng-message="required">Field is required</div>');

    var container = createContainedElement($rootScope, {});
    $rootScope.form.$setSubmitted();
    $rootScope.$digest();

    expect(container.html()).toContain('Field is required');

    $rootScope.form.$submitted = false;
    $rootScope.$digest();

    expect(container.html()).not.toContain('Field is required');

    $rootScope.form.model.$setDirty();
    $rootScope.$digest();

    expect(container.html()).toContain('Field is required');
  });

  it('uses the message template if provided', function() {
    $templateCache.put('templates/messages.html',
      '<div ng-message="required">Required</div>');

    var container = createContainedElement($rootScope, {
      'messages-source': 'templates/messages.html'
    });
    $rootScope.form.$setSubmitted();
    $rootScope.$digest();

    expect(container.html()).toContain('Required');
  });

  it('overrides a default message with a template', function() {
    $templateCache.put('templates/messages.html',
      '<div ng-message="required">New message</div>');
    $templateCache.put('templates/error-messages.html',
      '<div ng-message="required">Old message</div>');

    var container = createContainedElement($rootScope, {
      'messages-source': 'templates/messages.html'
    });
    $rootScope.form.$setSubmitted();
    $rootScope.$digest();

    expect(container.html()).toContain('New message');
  });

});
