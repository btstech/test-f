'use strict';

describe('E-Zerowaste App Core', function() {
  function changeLanguage(name) {
    browser.setLocation('app/settings');
    element(by.model('settings.language'))
      .element(by.cssContainingText('option', name)).click();
  }

  beforeEach(function() {
    browser.get('/');
  });

  it('should change the language', function() {
    changeLanguage('English');
    browser.setLocation('app/settings');
    expect(element(by.id('languageLabel')).getText()).toBe('Language');

    changeLanguage('Español');
    browser.setLocation('app/settings');
    expect(element(by.id('languageLabel')).getText()).toBe('Idioma');
  });

  it('should translate view titles (Bug #1)', function() {
    /**
    * Bug #1: https://e-zerowaste.visualstudio.com/DefaultCollection/E-Zerowaste%20App/_workitems#_a=edit&id=1&fullScreen=false
    */

    changeLanguage('English');
    browser.setLocation('app/main');
    changeLanguage('Español');
    browser.setLocation('app/main');

    expect($('[nav-bar=active] .title.header-item').getText())
      .toBe('Página principal');
  });
});
