/* globals require: false , console: false, process: false, __dirname: false */
/* jshint strict: false */

var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sh = require('shelljs');
var gettext = require('gulp-angular-gettext');
var inject = require('gulp-inject');
var jscs = require('gulp-jscs');
var jshint = require('gulp-jshint');
var mainBowerFiles = require('main-bower-files');
var fs = require('fs');
var del = require('del');
var runSequence = require('run-sequence');
var merge2 = require('merge2');
var karma = require('karma');
var exec = require('child_process').exec;

var paths = {
  sass: ['./app/scss/**/*.scss'],
  appJs: ['./app/js/**/*.js'],
  testJs: ['./test/**/*.js']
};
paths.allJs = paths.appJs.concat(paths.testJs);

function getVendorFiles() {
  return mainBowerFiles().map(function(path) {
    var minifiedPath = path.replace(/\.([^.]+)$/g, '.min.$1');

    try {
      fs.statSync(minifiedPath);
      return minifiedPath;
    } catch (e) {
      // Does not exist, return the original path
      return path;
    }
  });
}

function getKarmaFiles() {
  var vendorFiles = getVendorFiles().filter(function(path) {
    return path.endsWith('.js');
  });

  return vendorFiles.concat(['app/lib/angular-mocks/angular-mocks.js', 'www/js/**/*.js',
    'test/spec/**/*.js']);
}

gulp.task('default', ['build']);

gulp.task('index', function() {
  var vendorStream = gulp.src(getVendorFiles(), {base: './app/lib'})
    .pipe(gulp.dest('./www/lib'));

  var appStream = gulp.src(paths.appJs)
    .pipe(gulp.dest('./www/js'));

  return gulp.src('./app/index.html')
    .pipe(inject(merge2(vendorStream, appStream), {
      // The two options below remove '/www' and make the paths render as relative from 'www'
      ignorePath: '/www',
      addRootSlash: false
    }))
    .pipe(gulp.dest('./www'));
});

gulp.task('sass', function() {
  return gulp.src(paths.sass)
    .pipe(sass({
      errLogToConsole: true
    }))
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({extname: '.min.css'}))
    .pipe(gulp.dest('./www/css/'));
});

gulp.task('watch', function() {
  gulp.watch(['./app/**/*', '!./app/lib/**/*', '!./app/scss/**/*'], ['build']);
  // Handle SASS separately to avoid fully reloading the page
  gulp.watch(paths.sass, ['sass']);
});

gulp.task('install', ['git-check'], function() {
  return bower.commands.install()
    .on('log', function(data) {
      gutil.log('bower', gutil.colors.cyan(data.id), data.message);
    });
});

gulp.task('git-check', function(done) {
  if (!sh.which('git')) {
    console.log(
      '  ' + gutil.colors.red('Git is not installed.'),
      '\n  Git, the version control system, is required to download Ionic.',
      '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
      '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
    );

    process.exit(1);
  }
  done();
});

gulp.task('pot', function() {
  return gulp.src(['app/templates/**/*.html', 'app/*.html'].concat(paths.appJs))
    .pipe(gettext.extract('template.pot', {}))
    .pipe(gulp.dest('./app/translations/'));
});

gulp.task('translations', function() {
  return gulp.src('app/translations/**/*.po')
    .pipe(gettext.compile({
      format: 'json'
    }))
   .pipe(gulp.dest('./www/translations/'));
});

gulp.task('style-check', function() {
  return gulp.src(paths.allJs)
    .pipe(jscs())
    .pipe(jscs.reporter())
    .pipe(jscs.reporter('fail'));
});

gulp.task('style-fix', function() {
  return gulp.src(paths.allJs, {base: '.'})
    .pipe(jscs({fix: true}))
    .pipe(jscs.reporter())
    .pipe(gulp.dest('.'));
});

gulp.task('lint', function() {
  return gulp.src(paths.allJs)
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'))
    .pipe(jshint.reporter('fail'));
});

gulp.task('www-files', function() {
  return gulp.src('./app/copy_to_www/**/*')
    .pipe(gulp.dest('./www'));
});

gulp.task('templates', function() {
  return gulp.src('./app/templates/**/*')
    .pipe(gulp.dest('./www/templates'));
});

gulp.task('clean', function() {
  return del([
    './www/**/*'
  ]);
});

gulp.task('build', function(callback) {
  runSequence('style-check', 'lint', ['sass', 'translations', 'www-files', 'index', 'templates'],
    callback);
});

gulp.task('clean-build', function(callback) {
  runSequence('clean', 'build', callback);
});

gulp.task('unit', ['build'], function(callback) {
  new karma.Server({
    configFile: __dirname + '/test/karma.conf.js',
    singleRun: true,
    files: getKarmaFiles()
  }, callback).start();
});

gulp.task('tdd', function() {
  gulp.watch(paths.allJs.concat('./app/**/*.html'), ['tdd-run']);
  new karma.Server({
    configFile: __dirname + '/test/karma.conf.js',
    files: getKarmaFiles(),
    autoWatch: false
  }).start();

  runSequence('tdd-run');
});

gulp.task('tdd-run', ['clean-build'], function(done) {
  // We call 'karma run' in a child process to avoid printing the test results twice (becase server
  // and runner are executed on this process).
  exec('karma run --port 9876', function(err, stdout, stderr) {
    done();
  });
});
